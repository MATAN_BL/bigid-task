How to install dependencies?
$ mvn clean install -U

How to run the jar file?
$ java -jar target/BigID-Task-1.0-SNAPSHOT-jar-with-dependencies.jar
Maven is planned to create a jar file that includes all the dependencies

Please mention that this project includes tests. In order to run them type:
$ mvn test

Please look at the configuration file src/main/resources/config.properties
It includes the following properties:
filePath: the path to the text file that is read
numOfWorkerThreads: number of threads in the thread pool
targetedWords: a list of words separated by comma (whitespaces are ignored) that the application
searches for in the text file
bulkSize: number of lines that will be sent to each thread
