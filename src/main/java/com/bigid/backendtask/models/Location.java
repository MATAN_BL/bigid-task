package com.bigid.backendtask.models;

import java.util.Objects;

public class Location {

  private int lineOffset;
  private int charOffset;

  public Location(int lineOffset, int charOffset) {
    this.lineOffset = lineOffset;
    this.charOffset = charOffset;
  }

  public int getLineOffset() {
    return lineOffset;
  }

  public int getCharOffset() {
    return charOffset;
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;
    Location location = (Location) o;
    return lineOffset == location.lineOffset &&
        charOffset == location.charOffset;
  }

  @Override
  public int hashCode() {
    return Objects.hash(lineOffset, charOffset);
  }

  @Override
  public String toString() {
    return "Location{" +
        "lineOffset=" + lineOffset +
        ", charOffset=" + charOffset +
        '}';
  }
}
