package com.bigid.backendtask.aggregators;

import com.bigid.backendtask.models.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class CustomAggregator implements IAggregator {

  private static final Logger logger = LoggerFactory.getLogger(CustomAggregator.class);

  public CustomAggregator() {}

  public Map<String, Set<Location>> aggregateCompletedFuturesResult(List<Future<Map<String, Set<Location>>>> futureList)  {
    logger.info("CustomAggregator is being executed");
    Map<String, Set<Location>> aggregatedResults = new HashMap<>();
    for (Future<Map<String, Set<Location>>> future : futureList) {
      try {
        addToAggregatedResults(future.get(), aggregatedResults);
      } catch (ExecutionException | InterruptedException e) {
        throw new RuntimeException("threads execution failed");
      }
    }
    return aggregatedResults;
  }

  private void addToAggregatedResults(Map<String, Set<Location>> futureResult, Map<String, Set<Location>> aggregatedResults) {
    futureResult.entrySet().stream().forEach(entry -> {
      if (aggregatedResults.get(entry.getKey()) != null) {
        aggregatedResults.get(entry.getKey()).addAll(entry.getValue());
      } else {
        aggregatedResults.put(entry.getKey(), entry.getValue());
      }
    });
  }
}
