package com.bigid.backendtask.aggregators;

import com.bigid.backendtask.models.Location;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

public interface IAggregator {

  Map<String, Set<Location>> aggregateCompletedFuturesResult(List<Future<Map<String, Set<Location>>>> futureList);

}
