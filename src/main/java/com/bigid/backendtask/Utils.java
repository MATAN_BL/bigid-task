package com.bigid.backendtask;

import com.bigid.backendtask.models.Location;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

public class Utils {

  private static final String CONFIG_FILE_PATH = "src/main/resources/config.properties";

  public static Properties readPropertiesFiles() {
    Properties props = new Properties();
    try (InputStream input = new FileInputStream(CONFIG_FILE_PATH)) {
      props.load(input);
      return props;
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static Set<String> getTargetedWords(Properties props) {
    String targetedWords = props.getProperty("targetedWords");
    return Arrays.asList(targetedWords.split(",")).stream().map(String::trim).collect(Collectors.toSet());
  }

  public static String prepareStringForOutput(Map<String, Set<Location>> output) {
    StringBuffer stringBuffer = new StringBuffer();
    output.entrySet().forEach(entry -> {
      stringBuffer.append(entry.getKey());
      stringBuffer.append(": ");
      stringBuffer.append(entry.getValue());
      stringBuffer.append("\n");
    });
    return stringBuffer.toString();
  }
}
