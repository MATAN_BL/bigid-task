package com.bigid.backendtask;

import com.bigid.backendtask.aggregators.CustomAggregator;
import com.bigid.backendtask.aggregators.IAggregator;
import com.bigid.backendtask.matchers.CustomMatcher;
import com.bigid.backendtask.matchers.IMatcher;
import com.bigid.backendtask.models.Location;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

  private static final Logger logger = LoggerFactory.getLogger(Main.class);

  private final ExecutorService executorService;
  private final IMatcher matcher;
  private final IAggregator aggregator;

  public Main(ExecutorService executorService, IMatcher matcher, IAggregator aggregator) {
    this.executorService = executorService;
    this.matcher = matcher;
    this.aggregator = aggregator;
  }

  public Map<String, Set<Location>> readFile(String filePath, Set<String> targetedWords, int bulkSize) {
    try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
      List<Future<Map<String, Set<Location>>>> endOfTasksFutures =
          executorService.invokeAll(createCallablesFromBulksOfData(br, targetedWords, bulkSize));
      executorService.shutdown();
      return aggregator.aggregateCompletedFuturesResult(endOfTasksFutures);
    } catch (IOException e) {
      logger.error("Reading {} failed", filePath, e);
      return null;
    }
    catch (InterruptedException e) {
      logger.error("executeService.invokeAll failed", e);
      return null;
    }
  }

  private List<Callable<Map<String, Set<Location>>>> createCallablesFromBulksOfData(BufferedReader br, Set<String> targetedWords, int bulkSize) throws IOException {
    List<String> textLines = new ArrayList<>();
    List<Callable<Map<String, Set<Location>>>> callables = new ArrayList<>();
    int lineNum = 0;
    String line;
    while ((line = br.readLine()) != null) {
      textLines.add(line);
      lineNum++;
      if (lineNum % bulkSize == 0) {
        final int bulkNum = lineNum / bulkSize - 1;
        addThreadToCallableList(textLines, targetedWords, bulkSize, bulkNum, callables);
      }
    }
    if (textLines.size() > 0) {
      addThreadToCallableList(textLines, targetedWords, bulkSize, lineNum / bulkSize, callables);
    }
    return callables;
  }

  private void addThreadToCallableList(List<String> textLines, Set<String> targetedWords, int bulkSize, int bulkNum, List<Callable<Map<String, Set<Location>>>> callables) {
    List<String> clonedTextLines = new ArrayList<>(textLines);
    Callable<Map<String, Set<Location>>> callable = (() -> {
      logger.info("Thread number {} is being executed", bulkNum);
      return matcher.getLocationsOfWord(clonedTextLines, targetedWords, bulkNum, bulkSize);
    });
    callables.add(callable);
    textLines.clear();
  }

  public static void main(String[] args) {
    Properties props = Utils.readPropertiesFiles();
    Set<String> targetedWords = Utils.getTargetedWords(props);

    int numOfWorkerThreads = Integer.parseInt(props.getProperty("numOfWorkerThreads"));
    int bulkSize = Integer.parseInt(props.getProperty("bulkSize"));
    ExecutorService executorService = Executors.newFixedThreadPool(numOfWorkerThreads);
    Main main = new Main(executorService, new CustomMatcher(), new CustomAggregator());

    String filePath = props.getProperty("filePath");
    logger.info("Main/readFile in invoked with filePath = {}, targetedWords = {}, bulkSize = {}", filePath, targetedWords, bulkSize);
    Map<String, Set<Location>> output = main.readFile(filePath, targetedWords, bulkSize);
    logger.info("********  Output: ********\n{}", Utils.prepareStringForOutput(output));
  }
}