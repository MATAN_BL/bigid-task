package com.bigid.backendtask.matchers;

import com.bigid.backendtask.models.Location;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CustomMatcher implements IMatcher{

  public CustomMatcher() {}

  @Override
  public Map<String, Set<Location>> getLocationsOfWord(List<String> textLines, Set<String> words, int bulkNum, int bulkSize) {
    Map<String, Set<Location>> result = words.stream().collect(Collectors.toMap(Function.identity(), __ -> new HashSet<>()));
    for (int lineNumInBulk = 0; lineNumInBulk < textLines.size(); lineNumInBulk++) {
      Map<String, Set<Location>> wordsInLine = findWordsInLine(words, textLines.get(lineNumInBulk), bulkNum * bulkSize + lineNumInBulk);
      wordsInLine.entrySet().stream().forEach(entry -> result.get(entry.getKey()).addAll(entry.getValue()));
    }
    return result;
  }

  private Map<String, Set<Location>> findWordsInLine(Set<String> words, String textLine, int lineNumber) {
    return words.stream().collect(Collectors.toMap(Function.identity(), word -> getWordLocationInLine(textLine, word, lineNumber)));
  }

  private Set<Location> getWordLocationInLine(String textLine, String word, int lineNumber) {
    Set<Location> wordLocationInLine = new HashSet<>();
    int i = 0;
    while (i >= 0) {
      i = textLine.indexOf(word, i);
      if (i != -1) {
        wordLocationInLine.add(new Location(lineNumber, i));
        i += word.length();
      }
    }
    return wordLocationInLine;
  }
}
