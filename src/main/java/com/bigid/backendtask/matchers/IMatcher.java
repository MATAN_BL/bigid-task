package com.bigid.backendtask.matchers;

import com.bigid.backendtask.models.Location;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IMatcher {

  Map<String, Set<Location>> getLocationsOfWord(List<String> textLines, Set<String> words, int bulkNum, int bulkSize);

}
