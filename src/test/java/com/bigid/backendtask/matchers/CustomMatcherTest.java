package com.bigid.backendtask.matchers;

import com.bigid.backendtask.models.Location;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class CustomMatcherTest {

  private static final int BULK_SIZE = 1000;
  private static final int BULK_NUM = 50;
  private CustomMatcher customMatcher;

  @Before
  public void setup() {
    customMatcher = new CustomMatcher();
  }

  @Test
  public void testGetLocationsOfWord() {
    List<String> textLines = List.of(
      "Arthur and Benny just visited Arthur's mother",
      "Some of Benny's best friends were met for lunch",
      "I have met neither Arthur nor Benny before"
    );
    Set<String> words = Set.of("Benny", "Arthur");
    Map<String, Set<Location>> locationOfTargetedWords = customMatcher.getLocationsOfWord(textLines, words, BULK_NUM, BULK_SIZE);

    Map<String, Set<Location>> expectedResult = Map.of(
        "Arthur", Set.of(new Location(50002, 19), new Location(50000, 0), new Location(50000, 30)),
        "Benny", Set.of(new Location(50002, 30), new Location(50000, 11), new Location(50001, 8))
    );
    Assert.assertEquals(expectedResult, locationOfTargetedWords);
  }
}
