package com.bigid.backendtask.aggregators;

import com.bigid.backendtask.models.Location;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import static org.junit.Assert.assertEquals;

public class CustomAggregatorTest {

  private IAggregator aggregator;

  @Before
  public void setup() {
     aggregator = new CustomAggregator();
  }

  @Test
  public void testAggregateCompletedFuturesResult() {

    Map<String, Set<Location>> dataForFuture1 =
        Map.of("Arthur", createMutableListOfLocations(new Location(1, 100), new Location(1, 101)));
    Future<Map<String, Set<Location>>> future1 = createFutureFromMatcherMap(dataForFuture1);
    Map<String, Set<Location>> dataForFuture2 =
        Map.of("Arthur", createMutableListOfLocations(new Location(1001, 201)),
            "Benny", createMutableListOfLocations(new Location(1001, 202), new Location(1001, 250)));
    Future<Map<String, Set<Location>>> future2 = createFutureFromMatcherMap(dataForFuture2);
    Map<String, Set<Location>> dataForFuture3 =
        Map.of("Benny", createMutableListOfLocations(new Location(2050,54), new Location(2060, 12)),
            "Arthur", createMutableListOfLocations(new Location(2200, 34)),
            "John", createMutableListOfLocations(new Location(2400, 98), new Location(2490, 33)));
    Future<Map<String, Set<Location>>> future3 = createFutureFromMatcherMap(dataForFuture3);
    List<Future<Map<String, Set<Location>>>> futureList = new ArrayList<>(List.of(future1, future2, future3));

    Map<String, Set<Location>> aggregationResult= aggregator.aggregateCompletedFuturesResult(futureList);

    assertEquals(Set.of("Arthur", "Benny", "John"), aggregationResult.keySet());

    Set<Location> arthurLocations = aggregationResult.get("Arthur");
    Assert.assertTrue(arthurLocations.contains(new Location(1, 100)));
    Assert.assertTrue(arthurLocations.contains(new Location(1, 101)));
    Assert.assertTrue(arthurLocations.contains(new Location(1001, 201)));
    Assert.assertTrue(arthurLocations.contains(new Location(2200, 34)));

    Set<Location> bennyLocations = aggregationResult.get("Benny");
    Assert.assertTrue(bennyLocations.contains(new Location(1001, 202)));
    Assert.assertTrue(bennyLocations.contains(new Location(1001, 250)));
    Assert.assertTrue(bennyLocations.contains(new Location(2050, 54)));
    Assert.assertTrue(bennyLocations.contains(new Location(2060, 12)));

    Set<Location> johnLocations = aggregationResult.get("John");
    Assert.assertTrue(johnLocations.contains(new Location(2400, 98)));
    Assert.assertTrue(johnLocations.contains(new Location(2490, 33)));
  }

  private Future<Map<String, Set<Location>>> createFutureFromMatcherMap(Map<String, Set<Location>> matcherMap) {
    Future<Map<String, Set<Location>>> future = new FutureTask<>(() -> matcherMap);
    ((FutureTask<Map<String, Set<Location>>>) future).run();
    return future;
  }

  private Set<Location> createMutableListOfLocations(Location ... locations) {
    Set<Location> mutableLocationList = new HashSet<>();
    for (Location loc : locations) {
      mutableLocationList.add(loc);
    }
    return mutableLocationList;
  }

}
