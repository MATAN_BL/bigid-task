package com.bigid.backendtask;

import com.bigid.backendtask.aggregators.CustomAggregator;
import com.bigid.backendtask.aggregators.IAggregator;
import com.bigid.backendtask.matchers.CustomMatcher;
import com.bigid.backendtask.matchers.IMatcher;
import com.bigid.backendtask.models.Location;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.mockito.Mockito.eq;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.spy;

public class IntegrationTest {

  private static final int NUM_OF_WORKER_THREADS = 3;
  private static final int BULK_SIZE = 2;
  private static final String filePath = "src/test/resources/sample.txt";

  @Test
  public void test() {

    Set<String> targetedWords = Set.of("John", "Donald");

    ExecutorService executorService = Executors.newFixedThreadPool(NUM_OF_WORKER_THREADS);
    IMatcher matcher = spy(new CustomMatcher());
    IAggregator aggregator = new CustomAggregator();
    Main main = new Main(executorService, matcher, aggregator);

    Map<String, Set<Location>> actualResult = main.readFile(filePath, targetedWords, BULK_SIZE);
    Map<String, Set<Location>> expectedResult = Map.of(
        "John", Set.of(new Location(0, 0), new Location(0, 11)),
        "Donald", Set.of(new Location(1, 0), new Location(0,29), new Location(4, 0))
    );
    Assert.assertEquals(expectedResult, actualResult);

    Mockito.verify(matcher).getLocationsOfWord(eq(List.of("John Hello John morning john Donald", "Donald")), eq(targetedWords), anyInt(), eq(BULK_SIZE));
    Mockito.verify(matcher).getLocationsOfWord(eq(List.of("Weather Yossi Cup bicycle breakfast", "Sun is shining today")), eq(targetedWords), anyInt(), eq(BULK_SIZE));
    Mockito.verify(matcher).getLocationsOfWord(eq(List.of("Donald john")), eq(targetedWords), anyInt(), eq(BULK_SIZE));
  }
}