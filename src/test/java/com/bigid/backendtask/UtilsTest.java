package com.bigid.backendtask;

import com.bigid.backendtask.models.Location;
import org.junit.Test;

import java.util.Map;
import java.util.Properties;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UtilsTest {

  @Test
  public void testGetTargetedWords() {
    Properties props = new Properties();
    props.setProperty("targetedWords", "David  , Levy");
    Set<String> targetedWords = Utils.getTargetedWords(props);
    assertEquals(Set.of("David", "Levy"), targetedWords);
  }

  @Test
  public void testPrepareStringForOutput() {
    String output = Utils.prepareStringForOutput(
        Map.of(
            "David", Set.of(new Location(1,10), new Location(2, 20)),
            "Levy", Set.of(new Location(3, 30), new Location(4, 40))
            )
    );
    assertTrue((output.contains("David: [Location{lineOffset=2, charOffset=20}, Location{lineOffset=1, charOffset=10}]")) ||
        (output.contains("David: [Location{lineOffset=1, charOffset=10}, Location{lineOffset=2, charOffset=20}]")));
    assertTrue((output.contains("Levy: [Location{lineOffset=4, charOffset=40}, Location{lineOffset=3, charOffset=30}]")) ||
        (output.contains("Levy: [Location{lineOffset=3, charOffset=30}, Location{lineOffset=4, charOffset=40}]")));
  }
}
